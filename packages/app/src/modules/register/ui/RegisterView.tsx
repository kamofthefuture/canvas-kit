import * as React from "react";
import { FormikErrors, FormikProps, Field, Formik } from "formik";
import { validUserSchema } from "@canvas/common";
import { View } from "react-native";
import { Card, Button, Text } from "react-native-elements";
import { InputField } from "../../shared/InputField";

interface FormValues {
  email: string;
  password: string;
}

interface Props {
  submit: (values: FormValues) => Promise<FormikErrors<FormValues> | any>;
}
export class RegisterView extends React.Component<FormikProps<FormValues> & Props> {
  render() {
    const { submit } = this.props
    return (
      <Formik
        validationSchema={validUserSchema}
        initialValues={{
          email: '',
          password: '',
        }}
        onSubmit={(values, { setErrors }) => {
          // not working
          // console.log('sumb');
          // const errors = submit(values);
          // console.log('errors');
          // if (errors) {
          //   setErrors(errors);
          // }
        }}
      >
        {(formikProps) => (
          <View style={{
            flex: 1,
            display: 'flex',
            justifyContent: 'space-around'
          }}>
            <Text
              style={{
                fontSize: 30,
                alignSelf: 'center'
              }}
            >Sign up</Text>
            <Card>
              <Field
                containerStyle={{ width: '100%' }}
                name="email"
                placeholder="Email"
                component={InputField}
                autoCapitalize="none"
              />
              <Field
                containerStyle={{ width: '100%' }}
                name="password"
                secureTextEntry={true}
                placeholder="Password"
                component={InputField}
                autoCapitalize="none"
              />
              <Button
                style={{
                  marginTop: 20
                }}
                title="Submit"
                onPress={async () => {
                  const { handleSubmit, values, setErrors, } = formikProps
                  handleSubmit(values as any);

                  const errors = await submit(values)
                  console.log(errors);
                  if (errors) {
                    setErrors(errors);
                  }
                }}
              />
            </Card>
          </View>
        )
        }
      </Formik>
    )
  }
}
