import * as React from "react";
import { graphql, ChildMutateProps } from "react-apollo";
import gql from "graphql-tag";
import { signup, signupVariables } from "./__generated__/signup";

interface Props {
  children: (
    data: { submit: (values: signupVariables) => Promise<null> }
  ) => JSX.Element | null;
}

class C extends React.PureComponent<ChildMutateProps<Props, signup, signupVariables>> {
  submit = async (values: signupVariables) => {
    console.log('values', values, this.props);

    const response = await this.props.mutate({
      variables: values
    });
    console.log("response: ", response);
    return null;
  };

  render() {
    return this.props.children({ submit: this.submit });
  }
}

const SIGNUP_MUTATION = gql`
  mutation signup($email: String!, $password: String!) {
    signup(email: $email, password: $password) {
      token
      user {
        email
      }
    }
  }
`;

export const RegisterController = graphql<Props, signup, signupVariables>(SIGNUP_MUTATION)(C);