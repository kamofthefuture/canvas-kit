import * as React from 'react';
import App, { Container } from 'next/app'
import BaseLayout from '../src/components/Layout';

class Layout extends React.Component {
  render() {
    const { children } = this.props
    return (
      <BaseLayout>
        {children}
      </BaseLayout>
    )
  }
}

interface MainAppProps { 
  Component: any;
  pageProps: any;
}

export default class MainApp extends App<MainAppProps, {}> {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <Container>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Container>
    )
  }
}