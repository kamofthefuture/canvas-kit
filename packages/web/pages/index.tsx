import React, { Component } from 'react';
import Link from 'next/link'
import styled from 'styled-components';
import { connectPage, redirect } from '../src/containers';

const Wrapper = styled.div`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  ${({ theme }) => theme.font};
`;

const Block = styled.span`
  background-color: red;
  padding: 1rem;
  color: #fff;
`;

class Welcome extends Component {
  static async getInitialProps(ctx) {
    console.log(ctx)
    if (ctx.me.id) {
      redirect(ctx, '/dashboard');
    }
    return {};
  }

  render() {
    return (
      <div>
        <Wrapper>
          <Block>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Modi illum, molestias voluptatibus rerum animi nam ea expedita natus placeat nobis voluptas?
            Voluptates nemo veritatis dignissimos repudiandae maiores doloribus inventore mollitia?
          </Block>
          <Block>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Modi illum, molestias voluptatibus rerum animi nam ea expedita natus placeat nobis voluptas?
            Voluptates nemo veritatis dignissimos repudiandae maiores doloribus inventore mollitia?
          </Block>
        </Wrapper>

        <Link href="about">
          <a>about</a>
        </Link>

        <style jsx>{`
          a {
            color: blue;
          }
        `}</style>
      </div>
    )
  }
};

export default connectPage(Welcome);
