import * as React from 'react';
import { connectPage } from '../src/containers';
import Ui from '../src/connectors/Register';

export interface RegisterPageProps {
}

class RegisterPage extends React.Component<RegisterPageProps, any> {
  render() {
    return (
      <Ui />
    );
  }
}

export default connectPage(RegisterPage);