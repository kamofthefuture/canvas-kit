import React from 'react';
import styled from 'styled-components';
import { ThemeProvider } from '../../styles/styled-components';
import theme from '../../styles/theme';

const Container = styled.div`
	font-family: ${({ theme }) => theme.font};
`;

const Layout = ({ children, title = "new app" }) => (
	<ThemeProvider theme={theme}>
		<Container>
			{children}
		</Container>
	</ThemeProvider>
);

export default Layout;
