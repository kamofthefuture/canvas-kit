import * as React from 'react';
import { RegisterController } from "@canvas/controller";
import RegisterView from './ui';

const Register = () => (
  <React.Fragment>
    <RegisterController>
      {({ submit }) => <RegisterView submit={submit} />}
    </RegisterController>
  </React.Fragment>
);

export default Register;