import * as React from 'react';
import { validUserSchema } from "@canvas/common";
import { Formik, Field, Form as F, FormikErrors, FormikProps } from 'formik';

import styled, { keyframes } from '../../styles/styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  > * {
    flex: 1;
  }
  padding: ${({ theme }) => theme.space[4]};
`;

const Form = styled(F)`
  display: flex;
  flex-direction: column;
  max-width: 450px;
  
  input {
    font-size: 1.2rem;
    outline: none;
    border: none;
    border-bottom: 2px solid;
    border-color: ${({ theme }) => theme.colors.grey1};
    transition: border-color 200ms ease;
    &:focus {
      border-color: ${({ theme }) => theme.colors.palm};
    }
  }

  label {
    margin-top: ${({ theme }) => theme.space[2]};
  }
`;

const Welcome = styled.h2`
  font-weight: ${({ theme }) => theme.fontWeights.light};
  margin-top: 0;
`;

const Submit = styled.button`
    font-size: 1rem;
    outline: none;
    border: none;
    border-radius: 4px;
    background: ${({ theme }) => theme.colors.gradient1};
    margin-top: ${({ theme }) => theme.space[2]};
    color: #fff;
    flex: 0 0 auto;
    padding: ${({ theme }) => theme.space[1]};
`;

const Error = styled.div`
    color: ${({ theme }) => theme.colors.error};
    animation: ${keyframes`
    10%, 90% {
      transform: translate3d(-1px, 0, 0);
    }
    
    20%, 80% {
      transform: translate3d(2px, 0, 0);
    }

    30%, 50%, 70% {
      transform: translate3d(-4px, 0, 0);
    }

    40%, 60% {
      transform: translate3d(4px, 0, 0);
    }
    `} 1s ease-in-out;
`;

export interface FormValues {
  email: string;
  password: string;
  name: string;
}

export interface RegisterViewProps {
  submit: (values: FormValues) => Promise<FormikErrors<FormValues> | null>;
}

class RegisterView extends React.Component<FormikProps<FormValues> & RegisterViewProps> {
  render() {
    return (
      <Container>
        <Welcome>
          Sign up for a new experience
        </Welcome>
        <Formik
          initialValues={{
            name: '',
            email: '',
            password: '',
          }}
          validationSchema={validUserSchema}
          onSubmit={async (values, { setErrors }) => {

            this.props.submit(values);
            const errors = await this.props.submit(values);

            if (errors) {
              setErrors(errors);
            }
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <label htmlFor="email">Email</label>
              <Field name="email" type="email" />
              {errors.email &&
                touched.email && (
                  <Error>{errors.email}</Error>
                )}
              <label htmlFor="password">password</label>
              <Field name="password" type="password" />
              {errors.password &&
                touched.password && (
                  <Error>{errors.password}</Error>
                )}
              <Submit type="submit">signup</Submit>
            </Form>
          )
          }</Formik>
      </Container>
    );
  }
}

export default RegisterView;