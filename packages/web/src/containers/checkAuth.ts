import * as gql from 'graphql-tag';

export default (context, apolloClient) => (
  apolloClient.query({
    query: gql`
      query me {
        me {
          id
          verified
        }
      }
    `
  }).then(({ data }) => {
    const { me } = data;
    if(me === null) { 
      return {};
    }

    return me;
  })
);