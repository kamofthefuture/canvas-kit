import withData from './withData'
import withSentry from './withSentry'

const withInitialProps = (PageComponent) => {
  const originalGetInitialProps = PageComponent.getInitialProps;

  PageComponent.getInitialProps = async (ctx, apolloClient) => { // eslint-disable-line no-param-reassign
    const {pathname,
      req, query, isServer, ...rest
    } = ctx;
    
    let props = {
      loggedIn: true
    };
    
    try {
      if (isServer) {
        // do global level server things
      }

      if (originalGetInitialProps) {
        props = await originalGetInitialProps(ctx);
      }
    } catch (e) {} // eslint-disable-line
    
    return { query, pathname, ...props };
  };

  return PageComponent;
};

export default PageComponent => withInitialProps(withData(withSentry(PageComponent)));
