import * as React from 'react';
import * as cookie from 'cookie';
import * as PropTypes from 'prop-types';
import { ApolloProvider, getDataFromTree } from 'react-apollo';
import * as Head from 'next/head';

import initApollo from './initApollo';
import checkAuth from './checkAuth';

function parseCookies(ctx = {}, options = {}) {
  return cookie.parse(
    // eslint-disable-next-line
    ctx.req
      ? ctx.req.headers.cookie
        ? ctx.req.headers.cookie
        : ''
      : document.cookie,
    options,
  );
}

export default ComposedComponent => class WithData extends React.Component {
  static displayName = `WithData(${ComposedComponent.displayName})`;
  static propTypes = {
    serverState: PropTypes.object.isRequired, //eslint-disable-line
  }

  static async getInitialProps(ctx) {
    let serverState = {};
    const apollo = initApollo({}, {
      getToken: () => parseCookies(ctx).token,
    });

    ctx.me = await checkAuth(ctx, apollo);

    let composedInitialProps = {};
    if (ComposedComponent.getInitialProps) {
      composedInitialProps = await ComposedComponent.getInitialProps(ctx, apollo);
    }

    if (!process.browser) {
      if (ctx.res && ctx.res.finished) {
        return;
      }

      const url = { query: ctx.query, pathname: ctx.pathname };
      try {
        const app = (
          <ApolloProvider client={apollo}>
            <ComposedComponent url={url} {...composedInitialProps} />
          </ApolloProvider>
        );
        await getDataFromTree(app, {
          router: {
            query: ctx.query,
            pathname: ctx.pathname,
            asPath: ctx.asPath,
          },
        });
      } catch (error) {
        console.error(error);
      }
      Head.rewind();
      serverState = apollo.cache.extract();
    }
    // eslint-disable-next-line
    return {
      serverState,
      ...composedInitialProps,
    };
  }

  constructor(props) {
    super(props);
    this.apollo = initApollo(this.props.serverState, {
      getToken: () => parseCookies().token,
    });
  }

  render() {
    return (
      <ApolloProvider client={this.apollo}>
        <ComposedComponent {...this.props} />
      </ApolloProvider>
    );
  }
};