import * as React from 'react';
import * as Raven from 'raven-js';

const SENTRY_DSN = '';

function withSentry(Child) {
  return class WrappedComponent extends React.Component {
    static getInitialProps(context) {
      if (Child.getInitialProps) {
        return Child.getInitialProps(context);
      }
      return {};
    }
    constructor(props) {
      super(props);

      this.state = {
        error: null
      };

      Raven.config(
        SENTRY_DSN
      ).install();
    }

    componentDidCatch(error, errorInfo) {
      this.setState({ error });
      console.error(error)
      Raven.captureException(JSON.stringify(error), { extra: JSON.stringify(errorInfo) });
    }

    render() {
      return <Child {...this.props} error={this.state.error} />
    }
  }
}

export default withSentry;