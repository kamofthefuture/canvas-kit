import * as styledComponents from 'styled-components';
import { ThemedStyledComponentsModule } from 'styled-components';

interface ThemeInterface {
  font: string;
  colors: {
    black: string;
    jetBlack: string;
    offWhite: string;
    primeWhite: string;
    grey1: string;
    grey2: string;
    grey3: string;
    grey4: string;
    palm: string;
    fresh: string;
    cherry: string;
    info: string;
    linkSpecial: string;
    linkRegular: string;
    ciroc: string;
    error: string;
    gradient1: string;
  }
  fontWeights: {
    bold: number;
    black: number;
    light: number;
  }
  space: string[];
  fontSizes: {
    tiny: string;
    small: string;
    normal: string;
    big: string;
    super: string;
  }
}

const {
  default: styled,
  css,
  injectGlobal,
  keyframes,
  ThemeProvider
} = styledComponents as ThemedStyledComponentsModule<ThemeInterface>;

export { css, injectGlobal, keyframes, ThemeProvider };
export default styled;
